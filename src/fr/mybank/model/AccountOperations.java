package fr.mybank.model;


import java.util.Date;

import fr.mybank.entity.Account;
import fr.mybank.entity.Operation;

public class AccountOperations extends Operation{
	

	public void addMoney(double amount, Account account) {
		System.out.println(account.getNameOfClass(account) + " starting: " + account.getBalance() + " €");
		AccountOperations opAdd = new AccountOperations();
		opAdd.setAmount(amount);
		opAdd.setDate(new Date());
		opAdd.setType("current");
		account.addOperations(opAdd);
		System.out.println(opAdd.getType()+" Add : +" + amount + " €");
		System.out.println(account.getNameOfClass(account) + " end balance: " + account.getBalance() + " €");
		System.out.println("--------------");
	}

	public void withdraw(double amount, Account account) {
		AccountOperations opWithdraw = new AccountOperations();
		System.out.println(account.getNameOfClass(account) + " starting: " + account.getBalance() + " €");
		System.out.println(opWithdraw.getType()+" Withdraw: -" + amount + " €");
				
		if (checkIfEnough(account,amount)) 
		{
			opWithdraw.setAmount((-1)*amount);
			opWithdraw.setDate(new Date());
			opWithdraw.setType("current");
			account.addOperations(opWithdraw);
			
		}
		else 
		{
			System.out.println("Not enough money to withdraw");
		}
			System.out.println(account.getNameOfClass(account) + " end balance: " + account.getBalance() + " €");
			System.out.println("--------------");
		
	}
	public void addInterest(Account account) {
		System.out.println(account.getNameOfClass(account) + " starting: " + account.getBalance() + " €");
		System.out.println("Add interest: " + account.getBalance() * account.getInterestPercentage() + " €");
		
		AccountOperations opInterest = new AccountOperations();
		opInterest.setAmount(account.getBalance() * account.getInterestPercentage());
		opInterest.setDate(new Date());
		opInterest.setType("current");
		account.addOperations(opInterest);
		System.out.println(account.getNameOfClass(account) + " end balance: " + account.getBalance() + " €");
		System.out.println("--------------");
	}

	public double transfertMoney (double amount, Account account) {
		
		if (checkIfEnough(account,amount))
		{
			System.out.println(account.getNameOfClass(account)+ " starting: "+ account.getBalance()+" €" );
			
			
			AccountOperations opTransfert = new AccountOperations();
			opTransfert.setAmount((-1)*amount);
			opTransfert.setDate(new Date());
			opTransfert.setType("current");
			account.addOperations(opTransfert);
			System.out.println(opTransfert.getType()+" " + amount + " €");
			System.out.println(account.getNameOfClass(account) + " end balance: "+ account.getBalance()+" €" );
			System.out.println("--------------");
			return amount;
		}
		else return 0;
		
	}

	private boolean checkIfEnough(Account account, double amount) {
		if (account.getBalance() >= amount)
			return true;
		else
			return false;
	}
}
