package fr.mybank.model;

import java.util.Date;

import fr.mybank.entity.Account;

public class SavingAccountOperations extends AccountOperations {

	public SavingAccountOperations() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public void addInterest(Account account) {
		System.out.println(account.getNameOfClass(account) + " starting: " + account.getBalance() + " €");
		System.out.println("Add interest: " + account.getBalance() * account.getInterestPercentage() + " €");
		
		SavingAccountOperations opInterest = new SavingAccountOperations();
		opInterest.setAmount(account.getBalance() * account.getInterestPercentage());
		opInterest.setDate(new Date());
		opInterest.setType("saving");
		account.addOperations(opInterest);
		System.out.println(account.getNameOfClass(account) + " end balance: " + account.getBalance() + " €");
		System.out.println("--------------");
	}
	@Override
	public void addMoney(double amount, Account account) {
		System.out.println(account.getNameOfClass(account) + " starting: " + account.getBalance() + " €");
		SavingAccountOperations opAdd = new SavingAccountOperations();
		opAdd.setAmount(amount);
		opAdd.setDate(new Date());
		opAdd.setType("saving");
		account.addOperations(opAdd);
		System.out.println(opAdd.getType()+" : +" + amount + " €");
		System.out.println(account.getNameOfClass(account) + " end balance: " + account.getBalance() + " €");
		System.out.println("--------------");
	}

	
}
