package fr.mybank.app;

import fr.mybank.entity.Account;
import fr.mybank.entity.SavingAccount;
import fr.mybank.model.AccountOperations;
import fr.mybank.model.SavingAccountOperations;

public class BankingApp {

	
	public BankingApp() {
	}


	public static void main(String[] args) {
		AccountOperations accountOps = new AccountOperations();
		SavingAccountOperations savingAccountOps = new SavingAccountOperations();
		Account compteAVue = new Account(500);
		
		System.out.println(compteAVue.getBalance()+" €" );
		
		accountOps.withdraw(200, compteAVue);
		
		accountOps.withdraw(400, compteAVue);
		
		accountOps.addMoney(700, compteAVue);
		
		accountOps.addInterest(compteAVue);
		
		SavingAccount savingAccount = new SavingAccount(100);
		System.out.println(savingAccount.getBalance()+" €" );
		savingAccountOps.addInterest(savingAccount);
		System.out.println("\nTransfert money from account to saving account");
		savingAccountOps.addMoney(accountOps.transfertMoney(500, compteAVue), savingAccount);
	}

}
