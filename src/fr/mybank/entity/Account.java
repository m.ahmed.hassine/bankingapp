package fr.mybank.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.mybank.model.AccountOperations;

public class Account {
	private double interestPercentage = 0.01;
	protected double balance;
	private long rib = 1L;
	private Client client;

	protected List<AccountOperations> Operations = new ArrayList<AccountOperations>();
	
	public Account() {
	}
		
	public long getRib() {
		return rib;
	}
	public String getNameOfClass(Account account) {
		return account.getClass().getSimpleName();
	}
	public Account(double balance) {
		AccountOperations op = new AccountOperations();
		op.setAmount(balance);
		op.setDate(new Date());
		op.setType("current");
		Operations.add(op);
	}
	public double getBalance() {
		balance = 0;
		for (AccountOperations operation : this.Operations) {
			if (operation.getType().equals("current"))
			balance +=	operation.getAmount();
		}
		return balance;
	}

	public double getInterestPercentage() {
		return interestPercentage;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public void setRib(long rib) {
		this.rib = rib;
	}
	public List<AccountOperations> getOperations() {
		return Operations;
	}
	public void addOperations(AccountOperations operation) {
		Operations.add(operation);
	}
	

	public Account(long rib, Client client) {
		super();
		this.rib = rib;
		this.client = client;
	}

	

}
