package fr.mybank.entity;

public class Client {
	private long id = 1L;
	private String name;
	private String surname;
	
	public Client() {
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

}
