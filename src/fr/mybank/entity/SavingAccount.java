package fr.mybank.entity;

import java.util.Date;
import fr.mybank.model.AccountOperations;
import fr.mybank.model.SavingAccountOperations;

public class SavingAccount extends Account {

	private double  interestPercentage = 0.03;
	private long id = 1L;
	private Client client;
	
	public SavingAccount() {
	}

	public SavingAccount(double balance) {
		SavingAccountOperations op = new SavingAccountOperations();
		op.setAmount(balance);
		op.setDate(new Date());
		op.setType("saving");
		addOperations(op);
	}
	public double getInterestPercentage() {
		return interestPercentage;
	}
	public double getBalance() {
		balance = 0;
		for (AccountOperations operation : Operations) {
			if (operation.getType().equals("saving"))
				balance +=	operation.getAmount();
			}
			return balance;
	}
	public long getId() {
		return id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void addOperationsSA(SavingAccountOperations operation) {
		Operations.add(operation);
	}
	

	public SavingAccount(long id, Client client, double balance) {
		super(balance);
		this.id = id;
		this.client = client;
	}

}
